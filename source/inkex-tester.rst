inkex.tester package
====================

Submodules
----------

inkex.tester.decorators module
------------------------------

.. automodule:: inkex.tester.decorators
    :members:
    :undoc-members:
    :show-inheritance:

inkex.tester.filters module
---------------------------

.. automodule:: inkex.tester.filters
    :members:
    :undoc-members:
    :show-inheritance:

inkex.tester.mock module
------------------------

.. automodule:: inkex.tester.mock
    :members:
    :undoc-members:
    :show-inheritance:

inkex.tester.svg module
-----------------------

.. automodule:: inkex.tester.svg
    :members:
    :undoc-members:
    :show-inheritance:

inkex.tester.word module
------------------------

.. automodule:: inkex.tester.word
    :members:
    :undoc-members:
    :show-inheritance:

inkex.tester.xmldiff module
---------------------------

.. automodule:: inkex.tester.xmldiff
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: inkex.tester
    :members:
    :undoc-members:
    :show-inheritance:
